# ÉNÉFTAY

_Implémentation libre de certification numérique d'images_

## Dépendances

Python 3 avec les packages :
- `PIL`
- `datetime`
- `babel`

## Usage

Voir `./eneftay.py --help`

## Test

Faire `./eneftay.py img/exemple.jpg`
