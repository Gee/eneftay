#!/usr/bin/python3

import sys
from PIL import Image, ImageDraw, ImageFont
from datetime import date
from babel.dates import format_date

owner = "Framasoft"
auth = "le Pére Noël"
punish = "recevra un pan-pan sur le cul-cul"
filename = ""

def cut_lines_to_match_width(text, font, max_width):
    text = [text]
    while max([d.textsize(t, font=font)[0] for t in text]) > max_text_w:
        nb_lines = len(text) + 1
        text = ' '.join(text)
        nb_letters = len(text)
        target_size = nb_letters / nb_lines
        words = text.split(' ')
        start = 0
        text = []
        for i in range(len(words)):
            if len(' '.join(words[start:i])) > target_size:
                text.append(' '.join(words[start:(i-1)]))
                start = i-1
        text.append(' '.join(words[start:]))
    return text

def help():
    print("Usage: " + sys.argv[0] + " image.jpg [options]")
    print("Options:")
    print(" -h  --help      Afficher cette aide")
    print(" -o  --owner     Nom du / de la propriétaire   (défaut = \"Framasoft\")")
    print(" -a  --auth      Autorité de certification     (défaut = \"le Pére Noël\")")
    print(" -p  --punish    Punition (défaut = \"recevra un pan-pan sur le cul-cul\")")
    print("Example: " + sys.argv[0] + " mon_dessin.jpg -o \"Alex Térieur\" -a \"le Grand Manitou\" -p \"devra brûler un cierge au Dieu du Copyright\"")
    exit()

if len(sys.argv) == 1:
    help()

for a in sys.argv:
    if a == '-h' or a == '--help':
        help()
              
for k, v in zip(sys.argv[:-1], sys.argv[1:]):
    if k == '-o' or k == '--owner':
        owner = v
    elif k == '-a' or k == '--auth':
        auth = v
    elif k == '-p' or k == '--punish':
        punish = v
    else:
        for ext in { "jpg", "jpeg", "png" }:
            if v.lower().endswith(ext):
                ifilename = v

print("Génération de eneftay:")
print(" * ifilename = " + ifilename)
print(" * propriétaire = " + owner)
print(" * autorité de certification = " + auth)
print(" * punition = " + punish)

ofilename = '.'.join(ifilename.split('.')[:-1]) + "-eneftay-approvede." + ifilename.split('.')[-1]

# Settings
background_color = (255, 241, 192)
width = 750
height = 500
max_img_w = 340
max_img_h = 400
max_text_w = 370
margin = 20
interline = 35
footer_interline = 15
jpg_quality = 65

output = Image.new('RGB', (width, height), background_color)

# Create header
header_text = "CERTIFICAT ÉNÉFTAY APPROUVÉDE"
header_font = ImageFont.truetype("fonts/Omega.ttf", 48)
d = ImageDraw.Draw(output)
header_w, header_h = d.textsize(header_text, font=header_font)
d.text(((width - header_w) // 2, margin / 2), header_text, fill=(0,0,0), font=header_font)

# Resize image
image = Image.open(ifilename)
if image.height > max_img_h:
    image = image.resize((round(image.width * max_img_h / image.height), max_img_h))
if image.width > max_img_w:
    image = image.resize((max_img_w, round(image.height * max_img_w / image.width)))
center_img_x = image.width // 2 + margin
center_img_y = height - (image.height // 2 + margin)
output.paste(image, (center_img_x - image.width // 2, center_img_y - image.height // 2))

# Create text
text = "Par le pouvoir du Énéftay, "
text += auth + " certifie que cette image est la propriétay exclusive de "
text += owner + " et que tout personne y attentant " + punish + "."
text_font = ImageFont.truetype("fonts/UnPilgi.ttf", 33)
text = cut_lines_to_match_width(text, text_font, max_text_w)

text_y = margin + header_h
for t in text:
    text_w, text_h = d.textsize(t, font=text_font)
    text_x = width - text_w - margin
    d.text((text_x, text_y), t, fill=(0,0,0), font=text_font)
    text_y += interline

# Create footer
today = date.today()
footer = "Délivré le " + format_date(today, format='long', locale='fr') + " par les autorités incompétentes des interwebz. Ne peut être dupliqué. Enfin à part par un copié-collé, mais ce serait du piratage et ce serait pas très gentil."
footer_font = ImageFont.truetype("fonts/PTN77F.ttf", 15)
footer = cut_lines_to_match_width(footer, footer_font, max_text_w)
footer.reverse()

text_y = height - margin
for t in footer:
    text_w, text_h = d.textsize(t, font=footer_font)
    text_x = width - text_w - margin
    d.text((text_x, text_y - text_h), t, fill=(0,0,0), font=footer_font)
    text_y -= footer_interline

# Tamponné, double-tamponné
tampon = Image.open("img/tampon.png")
output.paste(tampon, (margin + image.width - round(tampon.width * 0.6), center_img_y + image.height // 2 - tampon.height + margin // 2), tampon)

# Save output
output.save(ofilename, quality=jpg_quality)
print("Sauvegardé dans " + ofilename)
